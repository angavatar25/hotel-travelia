import React from "react";
import './App.scss';
import { NavLink, withRouter } from "react-router-dom";

class SideBar extends React.Component {
    goToEvents = () => {
        this.props.history.push('/event-mainpage');
    }
    render() {
        return (
            <div className="sidebar">
                <div className="container-fluid">
                    <div className="navbar navigation-bar">
                        <div className="logo">
                            <h1 className="name-logo"><strong>Travellia</strong></h1>
                        </div>
                        <div className="navbar-menu">
                            <NavLink className="link" to="/">Home</NavLink>
                            <NavLink className="link" to="/">Bookings</NavLink>
                            <NavLink className="link" to="/event-mainpage">Events</NavLink>
                            <NavLink className="link" to="/">Help</NavLink>
                            <NavLink className="link btn btn-primary" to="/">Log In</NavLink>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(SideBar);