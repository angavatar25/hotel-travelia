import React from 'react';
import logo from './logo.svg';
import './App.scss';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import HomePage from './Home';
import SideBar from './Sidebar';
import SearchResult from './SearchResult';
import Footer from './custom-component/footer';
import ResultDetail from './ResultDetail';
import EventMainPage from './event-mainpage';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <SideBar/>
        <Switch>
          <Route exact path = "/" render={(props) => <HomePage{...props}/>}></Route>
          <Route exact path = '/search-result' render={(props) => <SearchResult{...props}/>}></Route>
          <Route exact path = '/result-detail' render={(props) => <ResultDetail{...props}/>}></Route>
          <Route exact path = '/event-mainpage' render={(props) => <EventMainPage{...props}/>}></Route>
        </Switch>
        <Footer/>
      </BrowserRouter>
    </div>
  );
}

export default App;
