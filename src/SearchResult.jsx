import React from "react";
import './App.scss';
import CardSearchResult from "./custom-component/card-searchresult";


class SearchResult extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title_one: '',
            title_two: '',
            title_three: '',
            collections: ''
        }
    }

    componentDidMount() {
        this.collections();
    }
    
    collections = () => {   
        const collections = [
            {
                id: '1',
                collection_genre: 'Family Events'
            },
            {
                id: '2',
                collection_genre: 'Classic Music'
            },
            {
                id: '3',
                collection_genre: 'Orchestra'
            },
            {
                id: '4',
                collection_genre: 'Local Culinary'
            },
            {
                id: '5',
                collection_genre: 'Live Jazz Music'
            },
            {
                id: '5',
                collection_genre: 'Football Match'
            }
        ]

        const collectionArray = collections.map((genre) => {
            return {
                id: genre.id,
                text: genre.id
            }
        })
        this.setState({
            collections: collectionArray
        })
    }
    render() {
        return (
            <div className="travellia-search-result">
                <div className="container-fluid">
                    <div className="search-bar">
                        <div className="content-container" id="location">
                            <p>Location</p>
                            <span>London, UK</span>
                        </div>
                        <div className="content-container" id="categories">
                            <p>Categories</p>
                            <div className="categories-selection">
                                <span>Hotels</span>
                                <span>icon</span>
                            </div>
                        </div>
                        <div className="content-container" id="date">
                            <p>Date</p>
                            <div className="categories-selection">
                                <span>21 May - 24 May 2020</span>
                                <span>icon</span>
                            </div>
                        </div>
                        <div className="button-container">
                            <div className="btn btn-primary">Search</div>
                        </div>
                    </div>
                    <div className="search-filter" style={{fontWeight: 'bold', fontFamily: 'Neue Helvetica®'}}>
                        <h1 className="page-title">Search Result</h1>
                        <span className="sub-title">Filter by:</span>
                        <div className="filter-button">
                            <div className="row mx-0">
                                <div className="link btn btn-primary">Prices</div>
                                <div className="link btn btn-primary">Facilities</div>
                                <div className="link btn btn-primary">Amount of Beds</div>
                                <div className="link btn btn-primary">More Filter</div>
                            </div>
                        </div>
                    </div>
                    <div className="search-result">
                        <div className="row">
                            <div className="col-md-6">
                                <CardSearchResult 
                                    title="The Upper House"
                                    price="£160 / night"    
                                    address='The Green, Stoke on Trent, ST12 9AE'
                                    rating='4.2/5.0 (13 Reviews)'
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SearchResult;