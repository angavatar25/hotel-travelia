import React from "react";
import App from './App.scss';
import { NavLink, withRouter } from "react-router-dom";
import projectImages from '../src/images/imageArray'
import CardReview from "./custom-component/card-review";
import CardWithTitle from "./custom-component/card-withtitle";

class ResultDetail extends React.Component {
    render() {
        const image_1 = require('../src/images/castle-combe-1.jpg');
        const image_2 = require('../src/images/castle-combe-2.jpg');
        const image_3 = require('../src/images/castle-combe-3.jpg');
        const image_photographer = require('../src/images/image-photographer.jpg');
        return(
            <div className="travellia-result-detail">
                <div className="picture-detail-upper-section">
                    <div className="container-fluid">
                        <div className="content-container">
                            <div className="back-button">
                                <i className="fas fa-arrow-left"></i>
                                <h3 className="back-title">Back to result page</h3>
                            </div>
                            <div className="content-detail">
                                <div className="sub-parent">
                                    <h1 className="place-title">Castle Combe</h1>
                                    <p className="address-summary">Cotswolds Conservation Board, Cheltenham</p>
                                </div>
                            </div>
                            <div className="button-action">
                                <div className="row mx-0">
                                    <a href="" className="parent">
                                        <i class="far fa-bookmark icon"></i>
                                        <p>Save</p>
                                    </a>
                                    <a href="" className="parent">
                                        <i class="far fa-thumbs-up icon"></i>
                                        <p>Like</p>
                                    </a>
                                    <a href="" className="parent">
                                        <i class="fas fa-share icon"></i>
                                        <p>Share</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="photo-collection">
                    <div className="container-fluid">
                        <h2 className="title">Photo Collections</h2>
                        <div className="row">
                            <div className="col-md-4 image-wrapper">
                                <img src={image_1} className="image" alt=""/>
                            </div>
                            <div className="col-md-4 image-wrapper">
                                <img src={image_2} className="image" alt=""/>
                            </div>
                            <div className="col-md-4 image-wrapper">
                                <img src={image_3} className="image" alt=""/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="photographer-testimonial">
                    <div className="container-fluid">
                        <div className="title-container">
                            <h2 className="title">What photographers said</h2>
                        </div>
                        <div className="testimonial-container">
                            <div className="testimonial-parent">
                                <div className="image-container">
                                    <img src={image_photographer} className="image-photographer" alt="image_photograph"/>
                                </div>
                                <div className="content-container">
                                    <h2 className="title">Andy Hobbs</h2>
                                    <span className="city">From Lewisham, London</span>
                                    <div className="border-line"></div>
                                    <p className="photographer-comments">
                                        Shimmering and Glorious
                                    </p>
                                </div>
                            </div>
                            <div className="testimonial-parent">
                                <div className="image-container">
                                    <img src={image_photographer} className="image-photographer" alt="image_photograph"/>
                                </div>
                                <div className="content-container">
                                    <h2 className="title">Timothy Darren Howard</h2>
                                    <span className="city">From Alberta, Canada</span>
                                    <div className="border-line"></div>
                                    <p className="photographer-comments">
                                        Extradiornary gorgeous village
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='review-section'>
                    <div className="container-fluid">
                        <div className='content'>
                            <h2 className="title">Review</h2>
                            <div className="row">
                                <div className="col-md-6"><CardReview/></div>
                                <div className="col-md-6"><CardReview/></div>
                                <div className="col-md-6"><CardReview/></div>
                                <div className="col-md-6"><CardReview/></div>
                            </div>
                            <div className="button-container-primary">
                                <div className="btn">
                                    <span>See all reviews</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="reach-more-section">
                    <div className="container-fluid">
                        <div className="content">
                            <h2 className="title">Reach more places</h2>
                            <div className="row">
                                <div className="col-md-3">
                                    <CardWithTitle
                                        title="The Manor"
                                        address='Hotels'
                                    />
                                </div>
                                <div className="col-md-3">
                                    <CardWithTitle
                                        title="Trafalgar House"
                                        address='Hotels'
                                    />
                                </div>
                                <div className="col-md-3">
                                    <CardWithTitle
                                        title="North Staffold"
                                        address='Hotels'
                                    />
                                </div>
                                <div className="col-md-3">
                                    <CardWithTitle
                                        title="The Manor"
                                        address='Hotels'
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(ResultDetail)