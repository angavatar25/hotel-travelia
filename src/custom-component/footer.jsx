import React from "react";
import '../App.scss';

export default class Footer extends React.Component {
    render() {
        return (
            <div className="travellia-footer">
                <div className="container-fluid">
                    <div className="content">
                        <div className="about-company">
                            <h4><strong>About our company</strong></h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard.</p>
                        </div>
                        <div className="social-media">
                            <p>Connect to our social media:</p>
                            <div className="icons">
                                <div className="middle">
                                    <div className="circle">
                                        <i class="fab fa-instagram"></i>
                                    </div>
                                </div>
                                <div className="middle">
                                    <div className="circle">
                                        <i class="fab fa-facebook-f"></i>
                                    </div>
                                </div>
                                <div className="middle">
                                    <div className="circle">
                                        <i class="fab fa-twitter"></i>
                                    </div>
                                </div>
                                <div className="middle">
                                    <div className="circle">
                                        <i class="fab fa-tumblr"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <span className="all-rights">© 2020 All Rights Reserved, Travelia. Co. Ltd</span>
                </div>
            </div>
        )
    }
}