import React from "react";
import '../App.scss';

export default class CardImageOverlay extends React.Component {
    render() {
        const image_hotel_1 = require('../images/hotel-1.jpg')
        const image_hotel_2 = require('../images/hotel-2.jpg')
        const image_hotel_3 = require('../images/hotel-3.jpg')
        return (
            <div className="card-image">
                <div className="image-container">
                    <img className="image-around" src={image_hotel_1} alt="hotel-image"/>
                    <div className="image-text">
                        <h2 className="title">North Stafford</h2>
                        <span className="price">£160 per night</span>
                        <div className="button-container">
                            <div className="btn btn-primary">Book</div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}