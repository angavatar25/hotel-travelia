import React from "react";
import '../App.scss';
import { NavLink, withRouter } from "react-router-dom";

class CardSearchResult extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: this.props.title,
            address: this.props.address,
            rating: this.props.rating,
            price: this.props.price,
        }
    }

    goToResultDetail = () => {
        this.props.history.push('/result-detail');
    }
    render() {
        const image_hotel_1 = require('../images/hotel-1.jpg')
        return (
            <div className="card-search-result">
                <div className="row mx-0">
                    <div className="col-md-3 px-0">
                        <div className="image-container">
                            <img src={image_hotel_1} className="card-image" alt="image"/>
                        </div>
                    </div>
                    <div className="col-md-9 px-0">
                        <div className="information-container">
                            <div className="title-and-price">
                                <h3 className="title">{this.state.title}</h3>
                                <h3 class="price">{this.state.price}</h3>
                            </div>
                            <span className="address">{this.state.address}</span> <br/>
                            <span className="ratings">{this.state.rating}</span>
                            <a href="" onClick={this.goToResultDetail} className="more-details-container">
                                <span>More Details</span>
                                <i class="fas fa-chevron-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(CardSearchResult);