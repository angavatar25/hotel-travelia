import React from "react";
import CustomStyle from '../CustomComponent.scss';
import { NavLink, withRouter } from "react-router-dom";

export default class CardReview extends React.Component {
    render() {
        return(
            <div className='cardReview'>
                <div className="card-item">
                    <div className="picture-with-name">
                        <div className="upper-container">
                            <div className="circle people-picture"></div>
                            <div className="name-container">
                                <p className="name-reviewer">Jason Clutterbuck</p>
                                <span className="date">From London, Feb 11 2020</span>
                            </div>
                        </div>
                        <div className="comment-container">
                            <div className="review-comment">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text.</p>
                            </div>
                            <div className="rating-stars">
                                <p>4 stars</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

