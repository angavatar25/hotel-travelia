import React from "react";
import '../App.scss';

export default class CardWithTitle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: this.props.title,
            address: this.props.address,
            description: this.props.description,
        }
    }

    render() {
        const image_example = require('../images/hotel-2.jpg');
        const description_state = this.state.description;
        let description_component;
        if (description_state === 'none') {
            description_component = (
                <div>
                    <p className="description">
                        
                    </p>
                </div>
            )
        } else description_component = (
                <div>
                    <p className="description">
                       {this.state.description} 
                    </p>
                </div>
        )
        return (
            <div className="card-withtitle">
                <div className="content-wrapper">
                    <img className="image-background" src={image_example} alt="image"/>
                    <div className="text-detail-wrapper">
                        <h2 className="title">{this.state.title}</h2>
                        <span className="address">{this.state.address}</span>
                        {/* <p className="description">
                            {this.state.description}
                        </p> */}
                        {description_component}
                        <div className="details-button-wrapper">
                            <a href="" className="details-button">
                                See details
                                <i class="fas fa-chevron-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}