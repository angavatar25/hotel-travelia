import React from "react";
import './App.scss';
import SideBar from "./Sidebar";
import CardImageOverlay from "./custom-component/card";
import Footer from "./custom-component/footer";
import { NavLink, withRouter } from "react-router-dom";
import CardWithTitle from "./custom-component/card-withtitle";

class EventMainPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title_one: 'Edm music festival',
            title_two: 'New dance song',
            event_date: '14 - 16 August 2021',
            location_date: 'O2 Arena, London'
        }
    }
    render() {
        const image_edm = require('./images/artists/edm_wallpaper.jpg')
        const billie_eilish = require('./images/artists/billie-eilish.jpg')
        const dua_lipa = require('./images/artists/dua_lipa.jpg');
        return(
            <div className="travellia-event-mainpage">
                <div className="picture-detail-upper-section">
                    <div className="container-fluid">
                        <div className="content-container">
                            <div className="back-button">
                                <i className="fas fa-arrow-left"></i>
                                <h3 className="back-title">Back to result page</h3>
                            </div>
                            <div className="content-detail">
                                <div className="sub-parent">
                                    <h1 className="place-title">{this.state.title_one}</h1>
                                    <h1 className="place-title">{this.state.title_two}</h1>
                                    <p className="address-summary">{this.state.event_date}, {this.state.location_date}</p>
                                </div>
                            </div>
                            <div className="button-container">
                                <div className="btn btn-primary">buy the ticket</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="search-section">
                    <div className="container-fluid">
                        <div className="search-bar">
                            <div className="content-container" id="location">
                                <p>Location</p>
                                <span>London, UK</span>
                            </div>
                            <div className="content-container" id="categories">
                                <p>Categories</p>
                                <div className="categories-selection">
                                    <span>Hotels</span>
                                    <span>icon</span>
                                </div>
                            </div>
                            <div className="content-container" id="date">
                                <p>Date</p>
                                <div className="categories-selection">
                                    <span>21 May - 24 May 2020</span>
                                    <span>icon</span>
                                </div>
                            </div>
                            <div className="button-container">
                                <div className="btn btn-primary">Search</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="recommendation-one">
                    <div className="container-fluid">
                        <h2 className="title">If you’re audiophiles</h2>
                        <div className="card-event-container">
                            <div className="card-event-wrapper">
                                <img src={image_edm} alt="image_artist" className="image-artist"/>
                                <div className="card-event-detail">
                                    <div className="card-dates">
                                        <span className="month">June</span>
                                        <h2 className="date">18</h2>
                                    </div>
                                    <div className="card-event-information">
                                        <span className="event-title">EDM Festival - O2 Arena London</span>
                                        <span className="event-information">Lorem Ipsum is simply dummy text of the printing</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(EventMainPage);