import React from "react";
import './App.scss';
import SideBar from "./Sidebar";
import CardImageOverlay from "./custom-component/card";
import Footer from "./custom-component/footer";
import { NavLink, withRouter } from "react-router-dom";
import CardWithTitle from "./custom-component/card-withtitle";

class HomePage extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            kategori_travelling: null
        }
    }

    componentDidMount() {
        this.categories();
    }
    categories = () => {
        const categories = [
            {
                key: "1",
                text: 'Hotel',
                imageUrl: require('./images/icons/hotel.svg')
            },
            {
                key: "2",
                text: 'Photograph',
                imageUrl: require('./images/icons/camera.svg')
            },
            {
                key: '3',
                text: 'Culinary',
                imageUrl: require('./images/icons/chef.svg')
            },
            {
                key: '4',
                text: 'Heritage',
                imageUrl: require('./images/icons/legal.svg')
            },
            {
                key: '5',
                text: 'Pub',
                imageUrl: require('./images/icons/alcohol.svg')
            }
        ]

        const kategori = categories.map((kategori) => {
            return {
                key: kategori.key,
                text: kategori.text,
                image: kategori.imageUrl
            }
        });
        
        this.setState({
            kategori_travelling: kategori
        })
        console.log(this.state.kategori_travelling)
    }
    searchPage = () => {
        this.props.history.push('/search-result')
    }
    resultPage = () => {
        this.props.history.push('/result-detail');
    }
    render() {
        const hotel_icon = require('./images/icons/hotel.svg');
        return(
            <div className="travellia-home">
                <div className="first-section">
                    <div className="container-fluid">
                        <div className="content">
                            <p>Find the most outrageous and exquisite destination you desire</p>
                            <h1 className="big-title">Discover great places</h1>
                            <p className="find-place">Find place to stay around your destination.</p>
                            <div className="search-bar">
                                <div className="content-container" id="location">
                                    <p>Location</p>
                                    <span>London, UK</span>
                                </div>
                                <div className="content-container" id="categories">
                                    <p>Categories</p>
                                    <div className="categories-selection">
                                        <span>Hotels</span>
                                        <span>icon</span>
                                    </div>
                                </div>
                                <div className="content-container" id="date">
                                    <p>Date</p>
                                    <div className="categories-selection">
                                        <span>21 May - 24 May 2020</span>
                                        <span>icon</span>
                                    </div>
                                </div>
                                <div className="button-container">
                                    <div className="btn btn-primary" onClick={this.searchPage}>Search</div>
                                </div>
                            </div>
                            <p className="hotel-recommendation">Here are our hotel recommendations around Stoke-On-Trent.</p>
                            <div className="image-wrapper">
                                <div className="row mx-0">
                                    <CardImageOverlay/>
                                    <CardImageOverlay/>
                                    <CardImageOverlay/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="second-section">
                    <div className="container-fluid">
                        <div className="section-recommendation">
                            <h2 className="title">Get your favourite spots</h2>
                            <p>Find your the finest spots to embrace the beauty of your destination.</p>
                            <div className="row mx-0">
                                {(this.state.kategori_travelling || []).map((key, item) => {
                                    return (
                                        <div className="box recommendation-wrapper">
                                            <div className="middle" key={item}>
                                                <div className="circle">
                                                    <img src={item.image} className="icon" alt="icon"/>
                                                </div>
                                                <span className="text">{item.text}</span>
                                            </div>
                                        </div>
                                    )
                                })}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="third-section">
                    <div className="container-fluid">
                        <div className="section-categories">
                            <h2 className="title">Photograph</h2>
                            <p className="sub-title">Capture your adventurous journey, give your best manoeuvre and share the moments together</p>
                        </div>
                        <div className="row">
                            <div className="col-md-3">
                                <CardWithTitle 
                                    title="Churnet Valley Railway"
                                    address="Staffordshire Moorlands."
                                    description="Appetite aims to get more people to experience and be inspired to develop and appetite for the arts by bring breath taking art events around the region."
                                />
                            </div>
                            <div className="col-md-3">
                                <CardWithTitle
                                    title="Churnet Valley Railway"
                                    address="Staffordshire Moorlands."
                                    description="Appetite aims to get more people to experience and be inspired to develop and appetite for the arts by bring breath taking art events around the region."
                                />
                            </div>
                            <div className="col-md-3">
                                <CardWithTitle
                                    title="Churnet Valley Railway"
                                    address="Staffordshire Moorlands."
                                    description="Appetite aims to get more people to experience and be inspired to develop and appetite for the arts by bring breath taking art events around the region."
                                />
                            </div>
                            <div className="col-md-3">
                                <div className="arrow-right">
                                    <a href="" onClick={this.resultPage}>
                                        <i class="fas fa-arrow-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="fourth-section">
                    <div className="container-fluid">
                        <div className="section-categories">
                            <h2 className="title">Discover events around Stoke-On-Trent.</h2>
                            <p className="sub-title">Are you an event-goer ? here are our recommendations just for you.</p>
                        </div>
                        <div className="row">
                            <div className="col-md-3">
                                <CardWithTitle/>
                            </div>
                            <div className="col-md-3">
                                <CardWithTitle/>
                            </div>
                            <div className="col-md-3">
                                <CardWithTitle/>
                            </div>
                            <div className="col-md-3">
                                <div className="arrow-right">
                                    <a href="">
                                        <i class="fas fa-arrow-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="newsletter-section">
                    <h1 className="title">Game for our new offers and newsletter ?</h1>
                    <h2 className="sub-title">Subscribe to our newsletter and get informed</h2>
                    <div className="input-email">
                        <input type="text" placeholder="Write your email"/>
                        <a href="" className="btn btn-primary">Subscribe</a>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(HomePage);